//
//  ConnectionManager.m
//  GettObjc
//
//  Created by Abhineet on 06/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import "ConnectionManager.h"
#include <objc/runtime.h>
#include "Session.h"

#define API_KEY @"t5ui5f0345r5asjorvjcw6hiv8yh6ko6r"
#define USERNAME @"abhineet06@gmail.com"
#define PASSWORD @"xRlILtei"


#define BASE_URL            @"http://open.ge.tt"
#define LOGIN               @"/1/users/login"

#define shares_url          @"http://open.ge.tt/1/shares?accesstoken=%@"
#define create_share_url    @"http://open.ge.tt/1/shares/create?accesstoken=%@"
#define delete_share_url    @"http://open.ge.tt/1/shares/%@/destroy?accesstoken=%@"
#define rename_share_url    @"http://open.ge.tt/1/shares/%@/update?accesstoken=%@"
#define create_file_url     @"http://open.ge.tt/1/files/%@/create?accesstoken=%@"

const NSInteger kTarget = 0;
const NSInteger kSelector = 1;
const NSInteger kResponseData = 2;


#define TARGET 0
#define SELECTOR 1
#define RESPONSE_DATA 2
#define CONNECTION_STATE 3


const int noresponce=999;

typedef NS_ENUM(NSInteger, ConnectionState)
{
    
    NO_RESPONSE,
    SUCCESS,
    FAILURE,
    NOT_STARTED
    
};

static ConnectionManager *instance;

@interface ConnectionManager()

-(NSArray *)deleteContextForConeection:(NSInteger)kId;
-(NSMutableArray *)getContextForURLRequest:(NSUInteger)kId;
-(NSMutableArray *)contextForTarget:(id)kTarget andSelector:(SEL)selector;

@property (nonatomic, strong) NSMutableDictionary *connectionDict;
@property (nonatomic, strong) NSString *accessToken;
@end


@implementation ConnectionManager

+(id)sharedInstance
{
    if(instance == NULL)
    {
        instance = [[ConnectionManager alloc] init];
        instance.connectionDict = [[NSMutableDictionary alloc] init];
    }
    return instance;
}


-(void)setContext:(NSMutableArray *)context ForConnection:(NSURLConnection *)connection
{
    [self.connectionDict setObject:context forKey:[NSNumber numberWithInteger:[connection hash]]];
}


-(NSMutableArray *)contextForTarget:(id)kTarget andSelector:(SEL)selector
{
    
    return  [NSMutableArray arrayWithArray:@[kTarget,NSStringFromSelector(selector),[NSMutableData data],[NSNumber numberWithInteger:noresponce]]];
}

-(NSMutableArray *)getContextForURLRequest:(NSUInteger)kId
{
    return [self.connectionDict objectForKey:[NSNumber numberWithInteger:kId]];
}


-(NSArray *)deleteContextForConeection:(NSInteger)kId
{
    return NULL;
}

-(void)appendData:(NSData *)responseData ToConnection:(NSURLConnection *)kConnection
{
    NSArray *context  = [self getContextForURLRequest:[kConnection hash]];
    [(NSMutableData *)context[RESPONSE_DATA] appendData:responseData];
    
}

-(void)setConnectionState:(NSInteger )kstate ForConnection:(NSURLConnection *)kConnection
{
    NSMutableArray *context  = [self getContextForURLRequest:[kConnection hash]];
    [context setObject:[NSNumber numberWithInteger:kstate] atIndexedSubscript:CONNECTION_STATE];
    
}








-(void)loginAndNotify:(id)target andSelector:(SEL)selector
{
    
    
    NSURL *loginURL  = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,LOGIN]];
    NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [loginRequest setHTTPMethod:@"POST"];
    
    
    //http body parameters for login
    /*
     {
     "apikey":"apitest", // use your own API key here
     "email":"apitest@ge.tt", // the users email
     "password":"secret" // the users password
     }
     
     // or using the refreshtoken
     
     {
     "refreshtoken":"r.test-app.u2DjvJQrKsvGGa9rw0..."
     }
     */
    
    
    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
    [httpBodyDict setObject:API_KEY forKey:@"apikey"];
    [httpBodyDict setObject:USERNAME forKey:@"email"];
    [httpBodyDict setObject:PASSWORD forKey:@"password"];
    
    NSError *error;
    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
    
    //NSLog(@"%@", [[NSString alloc] initWithData:httpbodyData encoding:NSStringEncodingConversionAllowLossy]);
    NSLog(@"%@",httpbodyData);
    
    [loginRequest setHTTPBody:httpbodyData];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:loginRequest delegate:self];
    [self.connectionDict setObject:[self contextForTarget:target andSelector:selector]
                            forKey:[NSNumber numberWithInteger:[connection hash]]];
    
    [connection start];
    
    NSLog(@"%@", [loginURL debugDescription]);
}






-(void)getSharesForCurrentUserWithTarget:(id)kTarget andSelector:(SEL)kSelector
{
    /*
     GET /1/shares?accesstoken={at}
     */
    
    
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:shares_url,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self setContext:[self contextForTarget:kTarget andSelector:kSelector] ForConnection:connection];
    [connection start];
    
    
}



-(void)createNewShareWithTitle:(NSString *)title target:(id)kTarget andSelector:(SEL)kSelector
{
    /*
     
     POST /1/shares/create?accesstoken={at}
     
     */
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:create_share_url,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
    [httpBodyDict setObject:title forKey:@"title"];
    
    NSError *error;
    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
    
    //TODO: check for error
    
    NSLog(@"%@",httpbodyData);
    
    [request setHTTPBody:httpbodyData];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self setContext:[self contextForTarget:kTarget andSelector:kSelector] ForConnection:connection];
    
    [connection start];
    
    
    
}


-(void)deleteShareWithTile:(NSString *)title target:(id)kTarget andSelector:(SEL)kSelector
{
    /*
     
     POST /1/shares/{sharename}/destroy?accesstoken={at}
     

     */
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:delete_share_url,title,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self setContext:[self contextForTarget:kTarget andSelector:kSelector] ForConnection:connection];
    
    [connection start];

    
}


-(void)renmaeShare:(NSString *)kShareName toNewTitle:(NSString*)kNewTitle target:(id)kTarget andSelector:(SEL)kSelector
{
    /*
     
     POST /1/shares/{sharename}/update?accesstoken={at}
     
     
     */
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:rename_share_url,kShareName,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
    [httpBodyDict setObject:kNewTitle forKey:@"title"];
    
    NSError *error;
    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
    
    //TODO: check for error
    
    NSLog(@"%@",httpbodyData);
    
    [request setHTTPBody:httpbodyData];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self setContext:[self contextForTarget:kTarget andSelector:kSelector] ForConnection:connection];
    
    [connection start];

    
}



#pragma mark - File Related operations
-(void)createFile:(NSString *)kTitle insideShare:(NSString *)kShareName target:(id)kTarget andSelector:(SEL)kSelector
{
    /*
     POST /1/files/{sharename}/create?accesstoken={at}
     
     {
     "filename":"myfile.txt",
     "session": "df32fsdfddf" // OPTIONAL! and only used by the live API
     }

     */
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:create_file_url,kShareName,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
    [httpBodyDict setObject:kTitle forKey:@"filename"];
    
    NSError *error;
    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
    
    //TODO: check for error
    
    NSLog(@"%@",httpbodyData);
    
    [request setHTTPBody:httpbodyData];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [self setContext:[self contextForTarget:kTarget andSelector:kSelector] ForConnection:connection];
    
    [connection start];

    
}




#pragma mark - NSURLConnectionDataDelegate methods

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    return request;
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%d", [(NSHTTPURLResponse*)response statusCode]);
    [self setConnectionState:[(NSHTTPURLResponse*)response statusCode] ForConnection:connection];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self appendData:data ToConnection:connection];
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSArray *context = [self getContextForURLRequest:[connection hash]];
    if(context)
    {
        NSLog(@"%@",context[SELECTOR]);
        [context[TARGET] performSelector:NSSelectorFromString(context[SELECTOR]) withObject:context[RESPONSE_DATA] withObject:context[CONNECTION_STATE]];
        
    }
    
    connection = nil;
}



@end






