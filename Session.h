//
//  Session.h
//  GettObjc
//
//  Created by Abhineet on 06/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session : NSObject
+(id)userInfoFromDictionary:(NSDictionary *)kDict;
+(id)accessToken;
+(id)userinfo;

@end
