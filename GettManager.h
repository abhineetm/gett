//
//  GettManager.h
//  GettObjc
//
//  Created by Abhineet on 06/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GettManager : NSObject
{
    
    NSMutableArray *_allshares;
    NSArray *_storage;
    NSMutableDictionary *_sharesAndFileDict;
}
@property (nonatomic, strong) NSArray *storage;
@property (nonatomic, strong) NSMutableArray *allShares;


+(id)sharedInstance;
-(void)loginWithTarget:(id)kTarget andSelector:(SEL)kSelector;
-(void)createNewShareWithTitle:(NSString *)kTitle andTarget:(id)kTarget andSelector:(SEL)kSelector;
-(void)deleteShare:(NSString *)kTitle andTarget:(id)kTarget andSelector:(SEL)kSelector;
-(void)renameShare:(NSString *)kShareName toNewTitle:(NSString *)kNewTitle andTarget:(id)kTarget andSelector:(SEL)kSelector;


/** File related operations **/

-(void)createNewFileWithTitle:(NSString *)kNewFileTitle insideShare:(NSString *)kShareName setTarget:(id)kTarget andSelector:(id)kSelector;

@end




@interface GTShare : NSObject
{
    NSString *_sharename;
    NSString *_readyState;
    NSString *_getturl;
    NSString *_createdDate;
    NSString *_title;
    NSMutableArray *_files;
    int _numberOfFiles;
}

-(id)initWithDictionary:(NSDictionary *)kDict;
@property (nonatomic, strong) NSString *shareName;
@property (nonatomic, strong) NSString *readyState;
@property (nonatomic, strong) NSString *getturl;
@property (nonatomic, strong) NSString *createdDate;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSMutableArray *files;
@property (nonatomic) int totalFiles;
@end



@interface GTFile : NSObject
{
    
    /*
     
     {
     blobs =                 (
     x675,
     50x36,
     185x135
     );
     created = 1337451788;
     downloads = 0;
     fileid = 1;
     filename = "sfdc_cloud_iPad_1024x768.jpg";
     getturl = "http://ge.tt/8Ke2iKD/v/1";
     readystate = uploaded;
     size = 211809;
     thumbs =                 {
     185x135 = "http://s3.kkloud.com/gett/8Ke2iKD/sfdc_cloud_iPad_1024x768.jpg.185x135.3j3z0nbtp722o6rvk407i1sfte53ik9.jpg";
     50x36 = "http://s3.kkloud.com/gett/8Ke2iKD/sfdc_cloud_iPad_1024x768.jpg.50x36.ubhwvt42y0e5qaorczf2viieqwr8uxr.jpg";
     };
     },
     
     
     */
    
    NSString            *_created;
    NSNumber            *_downloads;
    NSNumber            *_fileId;
    NSString            *_filename;
    NSString            *_getturl;
    NSString            *_readyState;
    NSNumber            *_size;
    NSMutableArray      *_thumbs;
}

@property (nonatomic, strong) NSString            *created;
@property (nonatomic, strong) NSNumber            *downloads;
@property (nonatomic, strong) NSNumber            *fileId;
@property (nonatomic, strong) NSString            *filename;
@property (nonatomic, strong) NSString            *getturl;
@property (nonatomic, strong) NSString            *readyState;
@property (nonatomic, strong) NSNumber            *size;
@property (nonatomic, strong) NSMutableArray      *thumbs;


-(id)initWithDictionary:(NSDictionary *)kDict;


@end