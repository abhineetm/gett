//
//  ConnectionManager.h
//  GettObjc
//
//  Created by Abhineet on 06/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
+(id)sharedInstance;
-(void)loginAndNotify:(id)target andSelector:(SEL)selector;



-(void)getSharesForCurrentUserWithTarget:(id)kTarget andSelector:(SEL)kSelector;
-(void)createNewShareWithTitle:(NSString *)title target:(id)kTarget andSelector:(SEL)kSelector;
-(void)deleteShareWithTile:(NSString *)title target:(id)kTarget andSelector:(SEL)kSelector;
-(void)renmaeShare:(NSString *)kShareName toNewTitle:(NSString*)kNewTitle target:(id)kTarget andSelector:(SEL)kSelector;



-(void)createFile:(NSString *)kTitle insideShare:(NSString *)kShareName target:(id)kTarget andSelector:(SEL)kSelector;
@end





