//
//  GettManager.m
//  GettObjc
//
//  Created by Abhineet on 06/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import "GettManager.h"
#import "ConnectionManager.h"
#import "Session.h"
#import <objc/runtime.h>
static GettManager *instance;
@interface GettManager()

@property (nonatomic, strong) ConnectionManager *con;
@property (nonatomic, strong) NSMutableDictionary *sharesAndFileDict;
@end


@implementation GettManager
@synthesize allShares = _allshares;
@synthesize storage = _storage;
@synthesize sharesAndFileDict = _sharesAndFileDict;
+(id)sharedInstance
{
    if(instance == NULL)
    {
        instance = [[GettManager alloc] init];
        instance.con = [ConnectionManager sharedInstance];
        NSLog(@"Initializing gett manager");
    }
    
    
    return instance;
}


-(NSMutableArray *)allShares
{
    if(_allshares == nil)
        _allshares = [[NSMutableArray alloc] init];
    
    return _allshares;
}


-(NSMutableDictionary *)sharesAndFileDict
{
    if(_sharesAndFileDict == nil)
        _sharesAndFileDict = [[NSMutableDictionary alloc] init];
    
    return _sharesAndFileDict;
}


#pragma mark - Utility Methods

-(void)displayAllShareNames
{
    for(GTShare *share in self.allShares)
        NSLog(@" %@ ",share.shareName);
}


-(void)displayAllSharesAndFiles
{
    
    for(GTShare *share in self.allShares)
    {
        NSLog(@" %@ ",share.shareName);
        for(GTFile *file in share.files)
        {
            NSLog(@"\t File name : %@",file.filename);
            NSLog(@"\t File Id   : %@",file.fileId);
        }
        
    }

}


#pragma mark - interface implementation

-(void)loginWithTarget:(id)kTarget andSelector:(SEL)kSelector
{
    [[ConnectionManager sharedInstance] loginAndNotify:self andSelector:@selector(loginResult:andResponseCode:)];
}


-(void)getAllShares:(id)kTarget andSelector:(SEL)kSelector
{
    [self.con getSharesForCurrentUserWithTarget:self andSelector:@selector(didGetShares:andResponseCode:)];
}

-(void)createNewShareWithTitle:(NSString *)kTitle andTarget:(id)kTarget andSelector:(SEL)kSelector
{
    [self.con createNewShareWithTitle:kTitle target:self andSelector:@selector(createShareResult:andResponseCode:)];
}

-(void)deleteShare:(NSString *)kTitle andTarget:(id)kTarget andSelector:(SEL)kSelector
{
    [self.con deleteShareWithTile:kTitle target:self andSelector:@selector(deleteShareResult:andResponseCode:)];
}

-(void)renameShare:(NSString *)kShareName toNewTitle:(NSString *)kNewTitle andTarget:(id)kTarget andSelector:(SEL)kSelector
{
    [self.con renmaeShare:kShareName toNewTitle:kNewTitle target:self andSelector:@selector(renameShareResult:andResponseCode:)];
}


-(void)createNewFileWithTitle:(NSString *)kNewFileTitle insideShare:(NSString *)kShareName setTarget:(id)kTarget andSelector:(id)kSelector
{
    [self.con createFile:kNewFileTitle insideShare:kShareName target:self andSelector:@selector(didCreateNewFile:andResponseCode:)];
}




#pragma mark - HTTP Response Callbacks



-(void)renameShareResult:(NSData *)responseData andResponseCode:(NSNumber *)kCode
{
    NSLog(@"%d",[kCode integerValue]);
    NSError *error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    NSLog(@"%@",json);
    
}



-(void)deleteShareResult:(NSData *)responseData andResponseCode:(NSNumber *)kCode
{
    NSLog(@"%d",[kCode integerValue]);
    NSError *error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    NSLog(@"%@",json);

}

-(void)createShareResult:(NSData *)responseData andResponseCode:(NSNumber *)kCode
{
    NSError *error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    NSLog(@"%@",json);
    
   
    
    //Add newly created Share to allShares.
    
    if([json isKindOfClass:[NSDictionary class]])
    {
        GTShare *newShare = [[GTShare alloc] initWithDictionary:(NSDictionary*)json];
        NSLog(@"%@",[newShare description]);
        
        
        [self.allShares addObject:newShare];
    }
    
    NSLog(@" TOTAL SHARES %d",[self.allShares count]);
    
}

-(void)loginResult:(NSData *)responseData andResponseCode:(NSNumber *)kCode
{
    
    if([kCode integerValue] != 200)
        return;
    
    
    NSError *error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    NSLog(@"%@",json);
    [Session userInfoFromDictionary:json];
    
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSDictionary *storageDict = [[json objectForKey:@"user"] objectForKey:@"storage"];
    if(self.storage == NULL)
    {
        
        self.storage = [[NSArray alloc] initWithObjects:[storageDict objectForKey:@"extra"],
                        [storageDict objectForKey:@"free"],
                        [storageDict objectForKey:@"limit"],
                        [storageDict objectForKey:@"used"],
                        nil];
    }
    //TODO: Remove this
    [self getAllShares:nil andSelector:nil];
    
    
}


-(void)didGetShares:(NSDate *)responseData andResponseCode:(NSNumber *)kCode
{
    
    if([kCode integerValue] != 200)
        return;
    
    NSError *error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    if([NSJSONSerialization isValidJSONObject:json])
    {
        for(id obj in json)
        {
            if([obj isKindOfClass:[NSDictionary class]])
            {
                GTShare *newShare = [[GTShare alloc] initWithDictionary:(NSDictionary*)obj];
                NSLog(@"%@",[newShare description]);
                
                
                if(self.allShares == NULL)
                    self.allShares = [[NSMutableArray alloc] init];
                
                [self.allShares addObject:newShare];
            }
        }
        
    }
    
    
    //TODO : remove this
    //[self displayAllSharesAndFiles];
    
    [self createNewFileWithTitle:@"test file3" insideShare:@"7yP3Vxf" setTarget:nil andSelector:nil];
    
}



-(void)didCreateNewFile:(NSData *)responseData andResponseCode:(NSNumber *)kCode
{
    if([kCode integerValue] != 200)
        return;
    
    NSError *error;
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          options:kNilOptions
                          error:&error];
    
    
    NSLog(@"%@",json);
    
    
    
    
    if([json isKindOfClass:[NSDictionary class]])
    {
        GTFile *newFile = [[GTFile alloc] initWithDictionary:(NSDictionary*)json];
        NSLog(@"%@",[newFile description]);
        
        
        
        /*
         upload =     {
         posturl = "http://w147560.blob2.ge.tt/streams/7yP3Vxf?filename=test%20file2&sig=-T6Q8z3nV6yf36kExQTDExL0u_9UCHC94tE";
         puturl = "http://w671271.blob3.ge.tt/streams/7yP3Vxf/test%20file2?sig=-T6Q8z3nV6yf36kExQTDExL0u_9UCHC94tE";
         };

         */
        
        NSDictionary *urls = [json objectForKey:@"upload"];
        NSString *postURL = [urls objectForKey:@"posturl"];
        NSString *putURL = [urls objectForKey:@"puturl"];
        //[self.allShares addObject:newShare];
        
        //post data to file
        
        NSURL *putReqURL = [NSURL URLWithString:putURL];
        NSMutableURLRequest *putRequest = [NSMutableURLRequest requestWithURL:putReqURL];
        NSString *text = @"abcdefgh";
        
        [putRequest setHTTPMethod:@"PUT"];
        [putRequest setHTTPBody:[text dataUsingEncoding:NSStringEncodingConversionAllowLossy]];
        

        
        
    }

//    if([NSJSONSerialization isValidJSONObject:json])
//    {
//        for(id obj in json)
//        {
//            if([obj isKindOfClass:[NSDictionary class]])
//            {
//                GTShare *newShare = [[GTShare alloc] initWithDictionary:(NSDictionary*)obj];
//                NSLog(@"%@",[newShare description]);
//                
//                
//                if(self.allShares == NULL)
//                    self.allShares = [[NSMutableArray alloc] init];
//                
//                [self.allShares addObject:newShare];
//            }
//        }
//        
//    }

}
@end









#pragma mark - GTShares


@interface GTShare()




@end



@implementation GTShare

const NSString *key_created = @"created";
const NSString *key_getturl  = @"getturl";
const NSString *key_readystate = @"readystate";
const NSString *key_title = @"title";
const NSString *key_sharename = @"sharename";
const NSString *key_file  = @"files";



@synthesize shareName = _sharename;
@synthesize getturl = _getturl;
@synthesize readyState = _readyState;
@synthesize title = _title;
@synthesize createdDate = _createdDate;
@synthesize totalFiles = _numberOfFiles;






-(id)initWithDictionary:(NSDictionary *)kDict
{
    
    self = [super init];
    if(self)
    {
        self.shareName =[kDict objectForKey:key_sharename];
        self.getturl = [kDict objectForKey:key_getturl];
        self.readyState = [kDict objectForKey:key_readystate];
        self.title = [kDict objectForKey:key_title];
        NSDictionary *filesDict  = [kDict objectForKey:key_file];
        self.totalFiles = 0;
        for(id file in filesDict)
        {
            
            if(self.files == NULL)
                self.files = [[NSMutableArray alloc] init];
            NSLog(@"%@",file);
            GTFile *newFile = [[GTFile alloc] initWithDictionary:(NSDictionary *)file];
            [self.files addObject:newFile];
            self.totalFiles++;
            NSLog(@" FILE : : %@",file);
            
        }
    }
    
    return self;
    
}



@end



#pragma mark - GTFile


@interface GTFile()

@end


@implementation GTFile



const NSString *key_downloads = @"downloads";
const NSString *key_fileid = @"fileid";
const NSString *key_filename = @"filename";
const NSString *key_readyState = @"readystate";
const NSString *key_size = @"size";
const NSString *key_thumbs = @"thumbs";



@synthesize created = _created;
@synthesize downloads = _downloads;
@synthesize fileId = _fileId;
@synthesize filename = _filename;
@synthesize getturl = _getturl;
@synthesize readyState = _readyState;
@synthesize size = _size;
@synthesize thumbs = _thumbs;



-(id)initWithDictionary:(NSDictionary *)kDict
{
    self = [super init];
    if (self)
    {
        
        NSNumberFormatter *formatter  = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        self.created = [kDict objectForKey:key_created];
        self.downloads = [kDict objectForKey:key_downloads];
        self.fileId = [kDict objectForKey:key_fileid];
        self.filename = [kDict objectForKey:key_filename];
        self.getturl = [kDict objectForKey:key_getturl];
        self.readyState = [kDict objectForKey:key_readyState];
        self.size = [kDict objectForKey:key_size];
        
        NSDictionary *thumbsArray = [kDict objectForKey:key_thumbs];
        if(thumbsArray)
        {
            self.thumbs = [NSMutableArray arrayWithObjects:[thumbsArray objectForKey:@"185x135"],
                           [thumbsArray objectForKey:@"50x36"],nil];
            
        }
        
        
    }
    return self;
}


@end

