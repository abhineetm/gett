//
//  Gett.m
//  GettObjc
//
//  Created by Abhineet on 10/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import "Gett.h"
#import "Session.h"


#define API_KEY     @"t5ui5f0345r5asjorvjcw6hiv8yh6ko6r"
#define USERNAME    @"abhineet06@gmail.com"
#define PASSWORD    @"xRlILtei"


#define login_url           @"http://open.ge.tt/1/users/login"
#define shares_url          @"http://open.ge.tt/1/shares?accesstoken=%@"
#define create_share_url    @"http://open.ge.tt/1/shares/create?accesstoken=%@"
#define delete_share_url    @"http://open.ge.tt/1/shares/%@/destroy?accesstoken=%@"
#define rename_share_url    @"http://open.ge.tt/1/shares/%@/update?accesstoken=%@"
#define create_file_url     @"http://open.ge.tt/1/files/%@/create?accesstoken=%@"

//GET /1/files/{sharename}/{fileid}

#define get_file_state_url  @"http://open.ge.tt/1/files/%@/%d"



@interface Gett()

@property (nonatomic, strong) NSOperationQueue *queue;

@end

@implementation Gett


-(NSMutableArray*)allShares
{
    /** lazy initialization **/
    if(_allShares == nil)
        _allShares = [[NSMutableArray alloc] init];
    return _allShares;
}

+(id)sharedInstance
{
    
    
    static Gett *instance = Nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Gett alloc] init];
    });

//    @synchronized(self){
//        if(instance == nil){
//            instance = [[Gett alloc] init];
//            instance.queue = [[NSOperationQueue alloc] init];
//        }
//    }
    return instance;
}




-(void)addFile:(GTFile *)kFile andShareName:(NSString *)kShareName
{
    NSString *uu = [NSString stringWithFormat:get_file_state_url,kShareName,[kFile.fileId integerValue]];
    NSURL *fileUpdateURL = [NSURL URLWithString:uu];
    NSMutableURLRequest *fileStateRequest = [NSMutableURLRequest requestWithURL:fileUpdateURL];
    [fileStateRequest setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:fileStateRequest
                                       queue:self.queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               NSLog(@"-----------%d",[(NSHTTPURLResponse *)response statusCode]);
                               if(!error && [(NSHTTPURLResponse *)response statusCode] == 200)
                               {
                                   
                                   NSError *parseError;
                                   NSDictionary* json = [NSJSONSerialization
                                                         JSONObjectWithData:data
                                                         options:kNilOptions
                                                         error:&parseError];
                                   
                                   if (!parseError) {
                                       
                                       if([json isKindOfClass:[NSDictionary class]])
                                       {
                                           [kFile updateFileWithDict:json];
                                           
                                           //add file to share
                                           for(GTShare *share in self.allShares)
                                           {
                                               if([share.shareName isEqualToString:kShareName])
                                               {
                                                   
                                                   [share addNewFile:kFile];
                                                   
                                               }
                                           }
                                           
                                       }
                                       
                                   }
                                   
                                   
                                   
                               }
                               else
                               {
                                   NSLog(@"Error::::::::::");
                               }
                           }];
    
    
}

-(void)loginAndNotify:(id)target andSelector:(SEL)selector
{
    
    
    NSURL *loginURL  = [NSURL URLWithString:login_url];
    NSMutableURLRequest *loginRequest = [NSMutableURLRequest requestWithURL:loginURL];
    [loginRequest setHTTPMethod:@"POST"];
    
    
    //http body parameters for login
    /*
     {
     "apikey":"apitest", // use your own API key here
     "email":"apitest@ge.tt", // the users email
     "password":"secret" // the users password
     }
     
     // or using the refreshtoken
     
     {
     "refreshtoken":"r.test-app.u2DjvJQrKsvGGa9rw0..."
     }
     */
    
    
    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
    [httpBodyDict setObject:API_KEY forKey:@"apikey"];
    [httpBodyDict setObject:USERNAME forKey:@"email"];
    [httpBodyDict setObject:PASSWORD forKey:@"password"];
    
    NSError *error;
    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
    
    //NSLog(@"%@", [[NSString alloc] initWithData:httpbodyData encoding:NSStringEncodingConversionAllowLossy]);
    NSLog(@"%@",httpbodyData);
    
    [loginRequest setHTTPBody:httpbodyData];
    
    
    
    
    [NSURLConnection sendAsynchronousRequest:loginRequest queue:self.queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!error)
         {
             NSLog(@" Login response code :%d", [(NSHTTPURLResponse*)response statusCode]);
             
             
             
             //extract user info
             NSError *error;
             NSDictionary* json = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&error];
             NSLog(@"%@",json);
             [Session userInfoFromDictionary:json];
             
             
             
             if(target && [target respondsToSelector:selector])
             {
                 [target performSelector:selector withObject:[Session userinfo] withObject:error];
                 
             }
             
         }
     }
     ];
    
    NSLog(@"%@", [loginURL debugDescription]);
}




-(void)getAllSharesAndFiles:(id)kTarget andSelector:(SEL)kSelector
{
    NSURL *url =[NSURL URLWithString:[NSString stringWithFormat:shares_url,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:self.queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         if(!error)
         {
             
             NSError *parseerror;
             NSDictionary* json = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&parseerror];
             if([NSJSONSerialization isValidJSONObject:json])
             {
                 
                 //create shares and all files
                 for(id obj in json)
                 {
                     if([obj isKindOfClass:[NSDictionary class]])
                     {
                         GTShare *newShare = [[GTShare alloc] initWithDictionary:(NSDictionary*)obj];
                         NSLog(@"%@",[newShare description]);
                         
                         @synchronized(self)
                         {
                             [self.allShares addObject:newShare];
                             
                         }
                         
                     }
                 }
                 
                 
                 //send response to target
                 if(kTarget && [kTarget respondsToSelector:kSelector])
                 {
                     [kTarget performSelector:kSelector withObject:self.allShares withObject:nil];
                 }
                 
             }
             
         }
         else
         {
             
         }
         
         
     }];
    
}


-(void)creatFileWithTitle:(NSString*)kNewFileTitle inShare:(NSString *)kShareName  AndFileData:(NSData *)kData withTarget:(id)kTarget andSelector:(SEL)kSelector
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:create_file_url,kShareName,[Session accessToken]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
    [httpBodyDict setObject:kNewFileTitle forKey:@"filename"];
    
    NSError *error;
    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
    
    //    check for error
    
    NSLog(@"%@",httpbodyData);
    
    [request setHTTPBody:httpbodyData];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:self.queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if(!error)
                               {
                                   
                                   
                                   
                                   
                                   
                                   NSError *parseerror;
                                   NSDictionary* json = [NSJSONSerialization
                                                         JSONObjectWithData:data
                                                         options:kNilOptions
                                                         error:&parseerror];
                                   if([NSJSONSerialization isValidJSONObject:json])
                                   {
                                       
                                       if([json isKindOfClass:[NSDictionary class]])
                                       {
                                           GTFile *file = [[GTFile alloc] initWithDictionary:(NSDictionary*)json];
                                           NSLog(@"%@",file.putURLString);
                                           
                                           
                                           NSURL *putURL =[NSURL URLWithString:file.putURLString];
                                           NSMutableURLRequest *fileUploadReq = [NSMutableURLRequest requestWithURL:putURL];
                                           [fileUploadReq setHTTPMethod:@"PUT"];
                                           [fileUploadReq setHTTPBody:kData];
                                           
                                           
                                           
                                           [NSURLConnection sendAsynchronousRequest:fileUploadReq
                                                                              queue:self.queue
                                                                  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                                                      
                                                                      if(!error)
                                                                      {
                                                                          NSLog(@"PDF    ------ %d",[(NSHTTPURLResponse *)response statusCode]);
                                                                          [self addFile:file andShareName:kShareName];
                                                                          
                                                                      }
                                                                      
                                                                  }];
                                           
                                       }
                                       
                                   }
                                   
                                   
                                   
                               }
                           }];
    
    
    
    
    
    
    
    
}



//-(void)creatFileWithTitle:(NSString*)kNewFileTitle inShare:(NSString *)kShareName withPlainText:(NSString *)kPlainText withTarget:(id)kTarget andSelector:(SEL)kSelector
//{
//    
//    
//    /*
//     POST /1/files/{sharename}/create?accesstoken={at}
//     
//     {
//     "filename":"myfile.txt",
//     "session": "df32fsdfddf" // OPTIONAL! and only used by the live API
//     }
//     
//     */
//    
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:create_file_url,kShareName,[Session accessToken]]];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"POST"];
//    
//    NSMutableDictionary *httpBodyDict = [[NSMutableDictionary alloc] init];
//    [httpBodyDict setObject:kNewFileTitle forKey:@"filename"];
//    
//    NSError *error;
//    NSData *httpbodyData = [NSJSONSerialization dataWithJSONObject:httpBodyDict options:NSJSONWritingPrettyPrinted error:&error];
//    
//    //    check for error
//    
//    NSLog(@"%@",httpbodyData);
//    
//    [request setHTTPBody:httpbodyData];
//    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:self.queue
//                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//                               if(!error)
//                               {
//                                   
//                                   //get the put url and send PUT the plain text
//                                   
//                                   /*
//                                    {
//                                    blobs =
//                                    (
//                                    );
//                                    created = 1368146443;
//                                    downloads = 0;
//                                    fileid = 3;
//                                    filename = "test file3";
//                                    fullname = "Abhineet Majrikar";
//                                    getturl = "http://ge.tt/7yP3Vxf/v/3";
//                                    meta =     {
//                                    };
//                                    readystate = remote;
//                                    sharename = 7yP3Vxf;
//                                    type = free;
//                                    upload =     {
//                                    posturl = "http://w256650.blob4.ge.tt/streams/7yP3Vxf?filename=test%20file3&sig=-T6RCFeQz_TnyDXzgW0KBrMa251oylhwLR0";
//                                    puturl = "http://w836005.blob2.ge.tt/streams/7yP3Vxf/test%20file3?sig=-T6RCFeQz_TnyDXzgW0KBrMa251oylhwLR0";
//                                    };
//                                    userid = EqAMX2OmRkht2I;
//                                    }
//                                    */
//                                   
//                                   
//                                   
//                                   
//                                   NSError *parseerror;
//                                   NSDictionary* json = [NSJSONSerialization
//                                                         JSONObjectWithData:data
//                                                         options:kNilOptions
//                                                         error:&parseerror];
//                                   if([NSJSONSerialization isValidJSONObject:json])
//                                   {
//                                       
//                                       if([json isKindOfClass:[NSDictionary class]])
//                                       {
//                                           GTFile *file = [[GTFile alloc] initWithDictionary:(NSDictionary*)json];
//                                           NSLog(@"%@",file.putURLString);
//                                           
//                                           
//                                           NSURL *putURL =[NSURL URLWithString:file.putURLString];
//                                           NSMutableURLRequest *fileUploadReq = [NSMutableURLRequest requestWithURL:putURL];
//                                           [fileUploadReq setHTTPMethod:@"PUT"];
//                                           [fileUploadReq setHTTPBody:[kPlainText dataUsingEncoding:NSStringEncodingConversionAllowLossy]];
//                                           [NSURLConnection sendAsynchronousRequest:fileUploadReq
//                                                                              queue:self.queue
//                                                                  completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//                                                                      
//                                                                      if(!error)
//                                                                      {
//                                                                          NSLog(@"%d",[(NSHTTPURLResponse *)response statusCode]);
//                                                                          [self addFile:file andShareName:kShareName];
//                                                                          
//                                                                      }
//                                                                      
//                                                                  }];
//                                           
//                                       }
//                                       
//                                   }
//                                   
//                                   
//                                   
//                               }
//                           }];
//    
//    
//    
//    
//    
//    
//}






@end





@interface GTShare()




@end



@implementation GTShare

const NSString *key_created = @"created";
const NSString *key_getturl  = @"getturl";
const NSString *key_readystate = @"readystate";
const NSString *key_title = @"title";
const NSString *key_sharename = @"sharename";
const NSString *key_file  = @"files";



@synthesize shareName = _sharename;
@synthesize getturl = _getturl;
@synthesize readyState = _readyState;
@synthesize title = _title;
@synthesize createdDate = _createdDate;
@synthesize totalFiles = _numberOfFiles;






-(id)initWithDictionary:(NSDictionary *)kDict
{
    
    self = [super init];
    if(self)
    {
        self.shareName =[kDict objectForKey:key_sharename];
        self.getturl = [kDict objectForKey:key_getturl];
        self.readyState = [kDict objectForKey:key_readystate];
        self.title = [kDict objectForKey:key_title];
        NSDictionary *filesDict  = [kDict objectForKey:key_file];
        self.totalFiles = 0;
        for(id file in filesDict)
        {
            
            if(self.files == NULL)
                self.files = [[NSMutableArray alloc] init];
            NSLog(@"%@",file);
            GTFile *newFile = [[GTFile alloc] initWithDictionary:(NSDictionary *)file];
            [self.files addObject:newFile];
            self.totalFiles++;
            NSLog(@" FILE : : %@",file);
            
        }
    }
    
    return self;
    
}

-(void)addNewFile:(GTFile *)kFile
{
    [self.files addObject:kFile];
    self.totalFiles = [self.files count];
}

@end



#pragma mark - GTFile


@interface GTFile()

@end


@implementation GTFile



const NSString *key_downloads = @"downloads";
const NSString *key_fileid = @"fileid";
const NSString *key_filename = @"filename";
const NSString *key_readyState = @"readystate";
const NSString *key_size = @"size";
const NSString *key_thumbs = @"thumbs";
const NSString *key_upload = @"upload";


@synthesize created = _created;
@synthesize downloads = _downloads;
@synthesize fileId = _fileId;
@synthesize filename = _filename;
@synthesize getturl = _getturl;
@synthesize readyState = _readyState;
@synthesize size = _size;
@synthesize thumbs = _thumbs;
@synthesize postURLString = _postURLString;
@synthesize putURLString = _putURLString;


-(id)initWithDictionary:(NSDictionary *)kDict
{
    self = [super init];
    if (self)
    {
        
        NSNumberFormatter *formatter  = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        self.created = [kDict objectForKey:key_created];
        self.downloads = [kDict objectForKey:key_downloads];
        self.fileId = [kDict objectForKey:key_fileid];
        self.filename = [kDict objectForKey:key_filename];
        self.getturl = [kDict objectForKey:key_getturl];
        self.readyState = [kDict objectForKey:key_readyState];
        self.size = [kDict objectForKey:key_size];
        
        NSDictionary *thumbsArray = [kDict objectForKey:key_thumbs];
        if(thumbsArray)
        {
            self.thumbs = [NSMutableArray arrayWithObjects:[thumbsArray objectForKey:@"185x135"],
                           [thumbsArray objectForKey:@"50x36"],nil];
            
        }
        
        
        if([self.readyState isEqualToString:@"remote"])
        {
            NSDictionary *urls = [kDict objectForKey:key_upload];
            self.putURLString = [urls objectForKey:@"puturl"];
            self.postURLString = [urls objectForKey:@"posturl"];
        }
        
        
    }
    return self;
}


-(void)updateFileWithDict:(NSDictionary *)kDict
{
    self.created = [kDict objectForKey:key_created];
    self.downloads = [kDict objectForKey:key_downloads];
    self.fileId = [kDict objectForKey:key_fileid];
    self.filename = [kDict objectForKey:key_filename];
    self.getturl = [kDict objectForKey:key_getturl];
    self.readyState = [kDict objectForKey:key_readyState];
    //self.size = [kDict objectForKey:key_size];
    
    NSDictionary *thumbsArray = [kDict objectForKey:key_thumbs];
    if(thumbsArray)
    {
        self.thumbs = [NSMutableArray arrayWithObjects:[thumbsArray objectForKey:@"185x135"],
                       [thumbsArray objectForKey:@"50x36"],nil];
        
    }
    
    if([self.readyState isEqualToString:@"remote"])
    {
        NSDictionary *urls = [kDict objectForKey:key_upload];
        self.putURLString = [urls objectForKey:@"puturl"];
        self.postURLString = [urls objectForKey:@"posturl"];
    }
    else
    {
        self.putURLString = nil;
        self.postURLString = nil;
    }
    
    
}





@end

