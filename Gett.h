//
//  Gett.h
//  GettObjc
//
//  Created by Abhineet on 10/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GTFile;

@interface Gett : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
@property (nonatomic, strong) NSMutableArray *allShares;

+(id)sharedInstance;

-(void)loginAndNotify:(id)target andSelector:(SEL)selector;

-(void)getAllSharesAndFiles:(id)kTarget andSelector:(SEL)kSelector;

-(void)creatFileWithTitle:(NSString*)kNewFileTitle inShare:(NSString *)kShareName  AndFileData:(NSData *)kData withTarget:(id)kTarget andSelector:(SEL)kSelector;


@end





@interface GTShare : NSObject
{
//    NSString *_sharename;
//    NSString *_readyState;
//    NSString *_getturl;
//    NSString *_createdDate;
//    NSString *_title;
//    NSMutableArray *_files;
//    int _numberOfFiles;
}

-(id)initWithDictionary:(NSDictionary *)kDict;
-(void)addNewFile:(GTFile *)kFile;

@property (nonatomic, strong) NSString *shareName;
@property (nonatomic, strong) NSString *readyState;
@property (nonatomic, strong) NSString *getturl;
@property (nonatomic, strong) NSString *createdDate;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSMutableArray *files;
@property (nonatomic) int totalFiles;
@end


@interface GTFile : NSObject
{
    
    /*
     
     {
     blobs =                 (
     x675,
     50x36,
     185x135
     );
     created = 1337451788;
     downloads = 0;
     fileid = 1;
     filename = "sfdc_cloud_iPad_1024x768.jpg";
     getturl = "http://ge.tt/8Ke2iKD/v/1";
     readystate = uploaded;
     size = 211809;
     thumbs =                 {
     185x135 = "http://s3.kkloud.com/gett/8Ke2iKD/sfdc_cloud_iPad_1024x768.jpg.185x135.3j3z0nbtp722o6rvk407i1sfte53ik9.jpg";
     50x36 = "http://s3.kkloud.com/gett/8Ke2iKD/sfdc_cloud_iPad_1024x768.jpg.50x36.ubhwvt42y0e5qaorczf2viieqwr8uxr.jpg";
     };
     },
     
     
     */
    
    NSString            *_created;
    NSNumber            *_downloads;
    NSNumber            *_fileId;
    NSString            *_filename;
    NSString            *_getturl;
    NSString            *_readyState;
    NSNumber            *_size;
    NSMutableArray      *_thumbs;
    NSString            *_putURLString;
    NSString            *_postURLString;
}

@property (nonatomic, strong) NSString            *created;
@property (nonatomic, strong) NSNumber            *downloads;
@property (nonatomic, strong) NSNumber            *fileId;
@property (nonatomic, strong) NSString            *filename;
@property (nonatomic, strong) NSString            *getturl;
@property (nonatomic, strong) NSString            *readyState;
@property (nonatomic, strong) NSNumber            *size;
@property (nonatomic, strong) NSMutableArray      *thumbs;
@property (nonatomic, strong) NSString            *putURLString;
@property (nonatomic, strong) NSString            *postURLString;


-(id)initWithDictionary:(NSDictionary *)kDict;
-(void)updateFileWithDict:(NSDictionary *)kDict;


@end