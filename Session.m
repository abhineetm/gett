//
//  Session.m
//  GettObjc
//
//  Created by Abhineet on 06/05/13.
//  Copyright (c) 2013 Abhineet. All rights reserved.
//

#import "Session.h"
static Session *currentUser;

/* user =     {
 cache =         {
 "chrome_extension" = 1;
 "chrome_extension_2" = 1;
 "chrome_extension_client" = 1;
 };
 created = 1298459912;
 downloads = 20;
 email = "abhineet06@gmail.com";
 files = 4;
 fullname = "Abhineet Majrikar";
 storage =         {
 extra = 1000000000;
 free = 2999625430;
 limit = 3000000000;
 used = 374570;
 };
 type = free;
 unread = 0;
 userid = EqAMX2OmRkht2I;
 };
 }*/
@interface Session()

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *userid;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSNumber *extra;
@property (nonatomic, strong) NSNumber *free;
@property (nonatomic, strong) NSNumber *limit;
@property (nonatomic, strong) NSNumber *used;
@end

@implementation Session
@synthesize accessToken,email,fullname;


+(id)userInfoFromDictionary:(NSDictionary *)kDict
{
    if(currentUser == NULL)
    {
        currentUser = [[Session alloc] init];
        [currentUser setAccessToken:[kDict objectForKey:@"accesstoken"]];
        
        NSDictionary *usrInfoDict = [kDict objectForKey:@"user"];
        
        [currentUser setEmail:[usrInfoDict objectForKey:@"email"]];
        [currentUser setFullname:[usrInfoDict objectForKey:@"fullname"]];
        [currentUser setUserid:[usrInfoDict objectForKey:@"userid"]];
        
        
        NSDictionary *storageDict = [kDict objectForKey:@"storage"];
        

        [currentUser setExtra:[storageDict objectForKey:@"extra"]];
        [currentUser setFree:[storageDict objectForKey:@"free"]];
        [currentUser setLimit:[storageDict objectForKey:@"extra"]];
        [currentUser setUsed:[storageDict objectForKey:@"extra"]];
        
        
    }
    
    return currentUser;
}


+(id)userinfo
{
    return currentUser;
}

+(id)accessToken
{
    return [currentUser accessToken];
}
@end
